import csv
import sys

def printMenu():
	print("1 for next entry")
	print("2 for delete entry")
	print("3 for menu")
	print("0 for finish")


if len(sys.argv) < 2:
	print("not enough positional arguments")
	exit(1)

filename = sys.argv[1]
result = []
inputs = ""
fieldnames = []
try:
	with open(filename) as csvf:
		reader = csv.DictReader(csvf)
		fieldnames = reader.fieldnames
		for row in reader:
			row["bkid"] = int(row["bkid"])
			row["availability"] = int(row["availability"])
			row["count"] = int(row["count"]) 
			result.append(row)

	printMenu()
	while(True):
		option = input();
		inputs += str(option) + "\n"
		option = int(option);
		if(option == 0):
			break;
		bkid = input()
		inputs += str(bkid) + "\n" 
		bkid = int(bkid)
		found = False
		for row  in result:
			if int(row["bkid"]) == bkid:
				found = True
				if(option == 1):
					row["availability"]+=1
				if(option == 2):
					row["availability"]-=1
		if not found:
			print("the book is not present in the file.")
		if(option != 1 and option != 2 and option != 0):
			printMenu()
			continue

		with open(filename,'w') as csvf:
			writer = csv.DictWriter(csvf,fieldnames = fieldnames)
			writer.writeheader()
			for row in result:
				writer.writerow(row)

except Exception as e:
	print("program crashed" + "\n" + str(e));
	logf = open("inputs.txt","w")
	logf.write(inputs);
	logf.close();
