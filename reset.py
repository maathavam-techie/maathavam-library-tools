import csv
import sys

if len(sys.argv) < 2:
	print("not enough positional arguments")
	exit(1)

filename = sys.argv[1]
result = []
fieldnames = []
with open(filename) as csvf:
	reader = csv.DictReader(csvf)
	fieldnames = reader.fieldnames
	for row in reader:
		row["availability"] = 0;
		result.append(row);

with open(filename,'w') as csvf:
	writer = csv.DictWriter(csvf,fieldnames = fieldnames)
	writer.writeheader()
	for row in result:
		writer.writerow(row)

print("reset availability of " + str(len(result)) + " records to 0");

